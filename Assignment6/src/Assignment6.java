import java.util.Scanner;

public class Assignment6{
	
	
	public String Days;
	String [] days ={"Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"};
	
	
	/**
	 * Initial Values
	 */
	public Assignment6() { 
		
		Days = days[0];
	}
	
/**
* Sets the current day 
* @param num refers to the numbers 0-6 that correspond with each day of the week
*/
	public void setToday(int num) {

		Days = days[num];	
	}
/**
 * Gets the day from setToday
 * @return Day of the week it is
 */
	public String getToday() {
		
		return Days;
	}
/**
+ * Gets the day after setToday
 * @param num refers to the numbers 0-6 that correspond with each day of the week
 * @return the following day
 */
	public String getTomorow(int num) {
		
		//tomorow gives the formula to get the number after setToday
		int tomorow = (num + 1) % 7;
		return days[tomorow];
	}
/**
 * Gets the day before setToday
 * @param num refers to the numbers 0-6 that correspond with each day of the week
 * @return the previous day
 */
	public String getYesterday(int num) {
		
		//yesterday gives the formula to get the number before setToday
		int yesterday = ((num + 7) % 7) - 1;
		return days[yesterday];
	}
	
/**
 * Adds any amount to the setDay to give you the newest day of the week.
 * @param num refers to the numbers 0-6 that correspond with each day of the week
 * @param addNum refers to the number the user enters that is added to num
 * @return Any day of the week that the user decides to add.
 */
	public String getGivenDay(int num, int addNum) {
		
		//given is the formula for getting future day
		int given = (num + addNum) % 7;
		return days[given];
	}
	
	public static void main(String[] args) {
		
		Assignment6 Assign6 = new Assignment6();
		Scanner keyboard = new Scanner(System.in);
		
		int num;
		int addNum;
		
		System.out.println("Enter the number that corresponds with the day it is:");
		num = keyboard.nextInt();
		Assign6.setToday(num);
		System.out.println("The day of the week is: " + Assign6.getToday());
		System.out.println("Tomorow is: " + Assign6.getTomorow(num));
		System.out.println("Yesterday was: " + Assign6.getYesterday(num));
		System.out.println("How many days do you want to pass?");
		addNum = keyboard.nextInt();
		System.out.println("It will be " + Assign6.getGivenDay(num, addNum));

		
	}

}