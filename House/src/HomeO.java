public class HomeO {
	
	public static void main(String[] args)
	{
		//This will ask the user to type in information
		/*Home yourHome = new Home();
		Scanner keyboard = new Scanner(System.in);
		System.out.println("Enter wall color:");
		String correctWalls = keyboard.nextLine();
		yourHome.setWalls(correctWalls);
		System.out.println("Enter floor type:");
		String correctFloor = keyboard.nextLine();
		yourHome.setFloor(correctFloor);
		System.out.println("Enter number of windows:");
		int correctWindows = keyboard.nextInt();
		yourHome.setWindows(correctWindows);
		System.out.println("Your room has");
		//This returns the information
		yourHome.writeOutput();
		*/
		
		//The first room in the house
		System.out.println("The first room has");
		Home firstHome = new Home();
		firstHome.setWalls("Yellow");
		firstHome.setFloor("Hardwood");
		firstHome.setWindows(1);
		firstHome.writeOutput();
		
		//the second room in the house
		System.out.println("The second room has");
		Home secondHome = new Home();
		secondHome.setWalls("Purple");
		secondHome.setFloor("Tiled");
		secondHome.setWindows(0);
		secondHome.writeOutput();

		//the third room in the house
		System.out.println("The third room has");
		Home thirdHome = new Home();
		thirdHome.setWalls("White");
		thirdHome.setFloor("Carpeted");
		thirdHome.setWindows(3);
		thirdHome.writeOutput();
	}
}
