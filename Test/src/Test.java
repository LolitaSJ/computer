import javax.swing.*;

import java.awt.*;
import java.awt.event.*;
import java.text.DecimalFormat;

public class Test extends JFrame implements ActionListener{
	private JLabel lblSizes, lblRAM, lblSpeed, lblType, lblDisk, lbl1, lbl2, lbl3, lblScore, lblWeight, lblOne,lblTwo, lblThree, lblFour, lblAve;
	private JTextField tfType, tfDisk, tfRAM, tfSizes, tfSpeed;
	private JButton button;
	private JPanel pnlSizes, pnlRAM, pnlSpeed, pnlType, pnlDisk, pnlWeight, pnlOne, pnlTwo, pnlThree, pnlFour, pnlAve;
	private static final int WIDTH = 600;
	private static final int HEIGHT = 400;
	
	public static String type;
	public static String disk;
	public static String sizes;
	public static String ram;
	public static String speed;
	
	public Test() {
		lbl1 = new JLabel("");
		lbl2 = new JLabel("");
		lbl3 = new JLabel("");
		lblScore = new JLabel("Create Your Computer:", SwingConstants.CENTER);
		
		lblType = new JLabel();
		pnlType = new JPanel();
		lblType.setText("Laptop, Desktop");
		pnlType.add(lblType, BorderLayout.CENTER);
		
		lblDisk = new JLabel();
		pnlDisk = new JPanel();
		lblDisk.setText("Disk, No Disk");
		pnlDisk.add(lblDisk, BorderLayout.CENTER);
		
		lblSizes = new JLabel();
		pnlSizes = new JPanel();
		lblSizes.setText("20 inch, 23 inch, 25 inch, 27 inch");
		pnlSizes.add(lblSizes, BorderLayout.CENTER);
		
		lblRAM = new JLabel();
		pnlRAM = new JPanel();
		lblRAM.setText("2 GB, 4 GB, 8 GB, 16 GB, 32 GB, 64 GB");
		pnlRAM.add(lblRAM, BorderLayout.CENTER);
		
		lblSpeed = new JLabel();
		pnlSpeed = new JPanel();
		lblSpeed.setText("20 GHz, 30 GHz");
		pnlSpeed.add(lblSpeed, BorderLayout.CENTER);
		
		lblWeight = new JLabel();
		pnlWeight = new JPanel();
		lblWeight.setText(" ");
		pnlWeight.add(lblWeight, BorderLayout.WEST);
	
		lblOne = new JLabel();
		pnlOne = new JPanel();
		lblOne.setText("Computer Type:");
		pnlOne.add(lblOne, BorderLayout.WEST);

		lblTwo = new JLabel();
		pnlTwo = new JPanel();
		lblTwo.setText("Disk:");
		pnlTwo.add(lblTwo, BorderLayout.WEST);
		
		lblThree = new JLabel();
		pnlThree = new JPanel();
		lblThree.setText("Monitor Size:");
		pnlThree.add(lblThree, BorderLayout.WEST);
		
		lblFour = new JLabel();
		pnlFour = new JPanel();
		lblFour.setText("RAM Amout:");
		pnlFour.add(lblFour, BorderLayout.WEST);
		
		lblAve = new JLabel();
		pnlAve = new JPanel();
		lblAve.setText("Processor Speed:");
		pnlAve.add(lblAve, BorderLayout.WEST);
	
		tfType = new JTextField();
		tfDisk = new JTextField();
		tfSizes = new JTextField();
		tfRAM = new JTextField();
		tfSpeed = new JTextField();
		
		//Creates calculation button
		button = new JButton("Get Receipt");
		button.addActionListener(this);
		
		//Creates and describes the pop-up panel
		setTitle("Purchasing a Computer");
		setSize(WIDTH, HEIGHT);
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		
		Container Pane = getContentPane();
		Pane.setLayout(new GridLayout(7,3));
		
		//Titles each column 'Score' and 'Weight'
		Pane.add(lbl1);
		Pane.add(lblScore);
		Pane.add(lblWeight);
		
		//Test and Weight 1
		Pane.add(lblOne);
		Pane.add(lblType);
		Pane.add(tfType);
		
		//Test and Weight 2
		Pane.add(lblTwo);
		Pane.add(lblDisk);
		Pane.add(tfDisk);
		
		//Test and Weight 3
		Pane.add(lblThree);
		Pane.add(lblSizes);
		Pane.add(tfSizes);
		
		//Test and Weight 4
		Pane.add(lblFour);
		Pane.add(lblRAM);
		Pane.add(tfRAM);
		
		//Shows Test and Weighted Average
		Pane.add(lblAve);
		Pane.add(lblSpeed);
		Pane.add(tfSpeed);
		
		//Calculation Button
		Pane.add(button);
		Pane.add(lbl3);
		
	}
	
	public void actionPerformed(ActionEvent e){
           type = Type.getText();
           disk = Disk.getText();
           sizes = Sizes.getText();
           ram = Ram.getText();
           speed = Speed.getText();
           
           setVisible(false);
           
        }



	}
	
