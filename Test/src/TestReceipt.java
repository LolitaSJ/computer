import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;


//WANT TO CREATE A LINE THAT TAKES THE SAVED USERNAME AND ADDS IT TO THE RECEIPT

public class TestReceipt extends JFrame implements ActionListener {
	
//These are the Strings i wanted to equal to the other classes	
	/*public static String Type;
	public static String Disk;
	public static String Sizes;
	public static String RAM;
	public static String Speed;
	*/
	
	private JLabel jlT, jlD, jlM, jlS, jlR;
	private JPanel jpT, jpD, jpM, jpS, jpR, jpB;
	private JButton button;
	
	public String[] type = {"Laptop", "Desktop"};
	public String[] disk = {"Disk", "No Disk"};
	public String[] ram = {"2", "4", "8", "16", "32", "64"};
	public String[] sizes = {"20", "23", "25", "27"};
	public String[] speed = {"20", "30"};
	
	
	public TestReceipt() {

		jlT = new JLabel();
		jpT = new JPanel();
		if ((Type.equals(type[0])) || (Type.equals(type[1]))) {
			jlT.setText(Type + " with");
		}
		else {
			jlT.setText("invalid Computer Type");
		}
		jpT.add(jlT, BorderLayout.WEST);
		
		jlD = new JLabel();
		jpD = new JPanel();
		if ((Disk.equals(disk[0])) || (Disk.equals(disk[1]))) {
			jlD.setText(Disk + ",");
		}
		else {
			jlD.setText("invalid Disk");
		}
		jpD.add(jlD, BorderLayout.WEST);
		
		jlM = new JLabel();
		jpM = new JPanel();
		if ((Sizes.equals(sizes[0])) || (Sizes.equals(sizes[1])) || (Sizes.equals(sizes[2])) || (Sizes.equals(sizes[3]))) {
			jlM.setText(Sizes + " inch Monitor, with");
		}
		else {
			jlM.setText("Invalid Size");
		}
		jpM.add(jlM, BorderLayout.WEST);
		
		jlS = new JLabel();
		jpS = new JPanel();
		if ((Speed.equals(speed[0])) || (Speed.equals(speed[1]))) {
			jlS.setText(Speed + " GHz processor speed.");
		}
		else {
			jlS.setText("Invalid Processor Speed");
		}
		jpS.add(jlS, BorderLayout.WEST);
		
		jlR = new JLabel();
		jpR = new JPanel();
		if ((RAM.equals(ram[0])) || (RAM.equals(ram[1])) || (RAM.equals(ram[2])) || (RAM.equals(ram[3])) || (RAM.equals(ram[4])) || (RAM.equals(ram[5]))) {
			jlR.setText(RAM + " GB of RAM,");
		}
		else {
			jlR.setText("Invalid RAM amount");
		}
		jpR.add(jlR, BorderLayout.WEST);
		
		button = new JButton("EXIT");
		jpB = new JPanel();
		button.addActionListener(this);
		jpB.add(button, BorderLayout.WEST);
		
		
		setTitle("Computer Receipt");
		setSize(400, 200);
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		Container Pane = getContentPane();
		Pane.setLayout(new GridLayout(6,1));
		
		//Username purchased
		Pane.add(jpT);
		Pane.add(jpD); 
		Pane.add(jpR); 
		Pane.add(jpM); 
		Pane.add(jpS);
		Pane.add(jpB);
		
	}


	public void actionPerformed(ActionEvent e) {
		setVisible(false);
		
	}
	
}
