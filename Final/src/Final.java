import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class Final extends JFrame implements ActionListener{
	private JPanel jp1 = new JPanel();
	private JPanel jp2 = new JPanel();
	private JPanel jp3 = new JPanel();
	private JLabel heading;
	private JLabel user;
	private JLabel pass;
	private JLabel responce;
	private JTextField userInput = new JTextField(20);
	//I changed JPasswordField to JTextField in order to make actionlistener function properly (i think)
	private JTextField userPass = new JTextField(20);
	private JButton done = new JButton("Done");

	public Final() {

//Constraints on JFrame
	    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(400,400);
		setLocationRelativeTo(null);
		setVisible(true);
		setLayout(new FlowLayout());
		
//Prompt the User
		heading = new JLabel("Create new Username/Password");
		//label
		jp1.add(heading);
		//presents label
		add(jp1);
		
//Create a Username
		user = new JLabel("Username");
		//label
		jp2.add(user);
		//text field parameter
		jp2.add(userInput);
		//presents label and field parameter
		add(jp2);
		
//Create a Password
		pass = new JLabel("Password");
		//label
		jp3.add(pass);
		//text field parameter
		jp3.add(userPass);
		//presents label and field parameter
		add(jp3);
		
//Submitting Username and Password		
		//adds button to the panel
		jp3.add(done);
		//Recognized that button is pressed
		done.addActionListener(this);
		
//Response message setup		
		responce = new JLabel();
		add(responce, BorderLayout.CENTER);
		
	}
	
		public void actionPerformed(ActionEvent e) {
			String savedUser;
			String savedPass;
			savedUser = userInput.getText();
			savedPass = userPass.getText();
//places restrictions on Username and password lengths
				if ((savedUser.length() < 5) || (savedUser.length() > 10) || (savedPass.length() < 5) || (savedPass.length() > 10)) {
					responce.setText("Invalid Username and/or Password. Try Again");
				}
				else if (((savedUser.length() >= 5) && (savedUser.length() <= 10)) && ((savedPass.length() >= 5) && (savedPass.length() <= 10))) {
					responce.setText("Welcome " + savedUser + ". Close Window to Proceed to Login");
				}
								
		}
		
		//public String getsavedUserText() {
		//	   return savedUser.getText();
		//	}
				
}


	
