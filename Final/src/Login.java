import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;


public class Login extends JFrame implements ActionListener{
	private JPanel jph = new JPanel();
	private JPanel jpu = new JPanel();
	private JPanel jpp = new JPanel();
	private JLabel jlh = new JLabel("Login Below");
	private JLabel jlu = new JLabel("Enter Username:");
	private JLabel jlp = new JLabel("Enter Password:");
	private JTextField juser = new JTextField(20);
	private JTextField jpass = new JTextField(20);
	private JButton jlogin = new JButton("Login");

	
	String Username = "";
	String Password = "";

	public Login() {
		//Constraints on JFrame
	    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(400,400);
		setLocationRelativeTo(null);
		setVisible(true);
		setLayout(new FlowLayout());
		
//Heading
		jph.add(jlh);
		add(jph);
		
//Username
		jpu.add(jlu);
		jpu.add(juser);
		add(jpu);
		
//Password
		jpp.add(jlp);
		jpp.add(jpass);
		add(jpp);
		
//Button
		jpp.add(jlogin);
		jlogin.addActionListener(this);
		
		
	}
	
	public void actionPerformed(ActionEvent e) {
		
	
	}
	
}
	
